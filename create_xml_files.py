import hashlib
import os

from common import get_channel_name_by_code
from create_csv_files import nomarlizeText
from path import base_enrichie, db_xmltv
import datetime
from xml.dom import minidom


def create_xml_from_metadata():
    for dir in os.listdir(base_enrichie):
        for f in os.listdir(db_xmltv):
            if f.endswith(".xml"):
                create_xml(db_xmltv + "\\" + f, dir)


# -----------------------------------------------------------------------------------------------------------------------
def create_date_from_start(start_time):
    start_time.strip().split('+')[0].strip()
    year_start = int(start_time[:4])
    month_start = int(start_time[4:6])
    day_start = int(start_time[6:8])
    hour_start = int(start_time[8:10])
    min_start = int(start_time[10:12])

    date = datetime(year_start, month_start, day_start, hour_start, min_start)
    return date.strftime("%Y%m%d_%H%M" + "00")


# -----------------------------------------------------------------------------------------------------------------------
def get_hashcode_from_xmltv(element):
    title = element.getElementsByTagName('title')[0].firstChild.nodeValue
    channel_code_hash = element.attributes['channel'].value.strip().split('.')[0].strip().lower()
    nomarlized_title = nomarlizeText(title).strip()

    value_hash = str(channel_code_hash) + str(nomarlized_title)
    hash_id = hashlib.sha1(value_hash.encode()).hexdigest()
    return hash_id


# -----------------------------------------------------------------------------------------------------------------------
def get_formatted_date(filepath):
    fileName = filepath.split('\\')[len(filepath.split('\\')) - 1]
    date = fileName.split('_')[0].strip()
    date_str = date.split('-')[0].strip() + date.split('-')[1].strip() + date.split('-')[2].strip()
    date_int = int(date_str)
    return date_int


# -----------------------------------------------------------------------------------------------------------------------
def create_xml(name, hashcode):
    if os.path.exists(name):
        xml_parse = minidom.parse(name)
        elem = xml_parse.getElementsByTagName("programme")

        for e in elem:
            # if hashcodes correspond and the start_time of the diffusion corresponds of the xml's date
            if get_hashcode_from_xmltv(e) == hashcode and int(
                    (e.getAttribute("start").strip().split('+')[0].strip())[:8]) <= get_formatted_date(name):

                output_xml_path = base_enrichie + hashcode + "\\" + create_date_from_start(
                    e.getAttribute("start").split('+')[0]) + "\\" + create_date_from_start(
                    e.getAttribute("start").split('+')[0]) + ".xml"

                # if the file doesn't already exist and the timestamp's repository exists
                if not os.path.exists(output_xml_path) and os.path.exists(
                        base_enrichie + hashcode + "\\" + create_date_from_start(
                                e.getAttribute("start").split('+')[0])):
                    start_time = e.attributes['start'].value
                    channel_name = get_channel_name_by_code(e.getAttribute("channel").split('.')[0].lower())
                    e.setAttribute("channel", channel_name)
                    xml_write = minidom.Document()
                    xml_write.appendChild(e)
                    with open(output_xml_path, "w", encoding="utf-8-sig") as xml_file:
                        xml_write.writexml(xml_file)


# -----------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':

    create_xml_from_metadata()
