import numpy as np
import pandas as pd
import os
from path import hashcodes_csv, base_enrichie, meta_data_csv


#-----------------------------------------------------------------------------------------------------------------------
#Function which creates hashcode's repositories from the csv file "hashcode.csv"
def create_repositories_from_hashcodes():

    dataframe = pd.read_csv(hashcodes_csv)
    hashcodes = np.array(dataframe)

    if not os.path.exists(base_enrichie):
        os.mkdir(base_enrichie)

    for i in range(0, hashcodes.shape[0]):

        output_repository = base_enrichie + ''.join(hashcodes[i])

        if not os.path.exists(output_repository):
            os.mkdir(output_repository)

        create_timestamp_repositories(hashcodes[i])

#-----------------------------------------------------------------------------------------------------------------------
#Function which creates timestamp's repositories in the repository of the hashcode
def create_timestamp_repositories(hashcode):

    df = pd.read_csv(meta_data_csv, sep=";")

    for row in df.itertuples():
        if ''.join(hashcode) == row.Hashcode:
            timestamp_rep = base_enrichie + hashcode + "\\" + str(row.Start)[0:8] + "_" + str(row.Start)[8:14]
            timestamp_rep_formatted = ''.join(timestamp_rep)

            if not os.path.exists(timestamp_rep_formatted):
                os.mkdir(timestamp_rep_formatted)


#-----------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':

     create_repositories_from_hashcodes()
