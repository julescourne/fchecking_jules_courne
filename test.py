#Tests are realized on a sample of one day.
import os.path
import pandas as pd
import numpy as np
from path import base_enrichie, meta_data_csv, hashcodes_csv


############################################# START TEST VIDEO / AUDIO #################################################

#Test which looks if all diffusions of 2022/02/01 have a segmented video
def test_video_1():
    for directory in os.listdir(base_enrichie):
        path_hash = base_enrichie + directory
        for sub_dir in os.listdir(path_hash):
            path_timestamp = path_hash + "\\" + sub_dir
            for timestamp_dir in os.listdir(path_timestamp):
                video_path = path_timestamp + "\\" + timestamp_dir
                if timestamp_dir[:8] == "20220201":
                    if not os.path.exists(video_path):
                        print("All videos are not segmented")
                        return False

    print("Succes ! All videos are segmented")
    return True


#Test which looks if all diffusions of 2022/02/01 have a segmented audio
def test_audio_1():
    for directory in os.listdir(base_enrichie):
        path_hash = base_enrichie + directory
        for sub_dir in os.listdir(path_hash):
            path_timestamp = path_hash + "\\" + sub_dir
            for timestamp_dir in os.listdir(path_timestamp):
                audio_path = path_timestamp + "\\" + timestamp_dir
                if timestamp_dir[:8] == "20220201":
                    if not os.path.exists(audio_path):
                        print("All audios are not segmented")
                        return False
    return True

############################################# END TEST VIDEO / AUDIO ###################################################






############################################# START TEST XML ###########################################################

def test_xml_1():
    for directory in os.listdir(base_enrichie):
        path_hash = base_enrichie + directory
        for sub_dir in os.listdir(path_hash):
            path_timestamp = path_hash + "\\" + sub_dir
            print(path_timestamp)
            for timestamp_dir in os.listdir(path_timestamp):
                xml_path = path_timestamp + "\\" + timestamp_dir + "\\" + timestamp_dir + ".xml"
                if timestamp_dir[:8] == "20220201":
                    if not os.path.exists(xml_path):
                        print("All xml are not created")
                        return False
    return True

############################################# END TEST XML #############################################################






############################################# START TEST REPOSITORIES ##################################################
#Test if there is a good number of timestamp repositories for the first day
def test_repositories_1():
    df = pd.read_csv(meta_data_csv, sep=";")
    cmpt_from_csv = 0
    cmpt_from_repo = 0

    for row in df.itertuples():
        if str(row.Start)[:8] == "20220201":
            cmpt_from_csv += 1

    for directory in os.listdir(base_enrichie):
        path_hash = base_enrichie + directory
        for sub_dir in os.listdir(path_hash):
            path_timestamp = path_hash + "\\" + sub_dir
            for timestamp_dir in os.listdir(path_timestamp):
                if timestamp_dir[:8] == "20220201":
                    cmpt_from_repo += 1

    #print(cmpt_from_repo, cmpt_from_csv)
    if cmpt_from_repo == cmpt_from_csv:
        print("Success ! Number of timestamp folder corresponds to metadata.csv")
        return True
    else:
        print("Number of timestamp folder are not corresponding to metadata.csv")
        return False

def test_repositories_2():
    df = pd.read_csv(hashcodes_csv)
    data = np.array(df)
    cmpt = 0
    for directory in os.listdir(base_enrichie):
        cmpt += 1
        if not directory in data:
            print("Folder name not in hashcode")
            return False
    hashcodes = []
    for i in data:
        hashcodes.append(''.join(i))
    if len(hashcodes) != len(set(hashcodes)):
        print("Doublon in hashcode")
        return False
    if cmpt != len(data):
        print("Folder length != hashcode length")
        return False
    return True

############################################# END TEST REPOSITORIES#####################################################
if __name__ == '__main__':

    test_video_1()

    #test_audio_1()

    #test_repositories_1()

    #test_repositories_2()

    #test_xml_1()
