Xmltv-db.zip :
    Partitionning des 24 chaines proposées par télérama du 17 décembre 2021 au 05 mai 2022, 20h par jour.

newsStorageKeyWords.xml :
    Fichier xml contanant le résultat quant à  la véracité de propos d’Hommes politiques à partir du site factoscope.
    Détails :
        News id = id de l’information.
        factoURL = url de l’article de factoscope. 
        Auteur = auteur du fait à vérifier
        Date = date à laquelle le fait a été dit.
        Titre = Le fait à vérifier.
        Veracite = Vrai / faux / imprécis
        mots-clefs = mots clés du fait.
        texteDescriptif = Texte qui décrit le contexte 
        sourcesURL= site du résultat du factchecking.

Orasis.pdf :

    Voir annexe chaine_traitement_STVD

    Cette chaine de traitement représente la construction de la base de données STVD.
    FP : Vidéos labélisées présentant un contenu visuel différent des segments recherchés.
    FN : Les FN sont des segments qui auraient été manqués par le protocole apparaissant dans les vidéos tests.
    VP : Vidéos présentant un contenu visuel correspondant aux segments recherchés.
    VN : ???

    Détails :
        C1 (La capture) :
        Ce fait à partir de la station TV. 

        C2 (La détection à grosse maille) :
        Extrait les vidéos VP.
        Fournit des métadonnées sur les principaux programmes (dates début, dates fin, titres, résumés ..).
        Détection de début et fin en fonction de génériques.

        C3 (La sélection aléatoire) :
        -	Extraction de la liste des programmes obtenu en C2.
        -	Rendre invalide les séquence avec un générique.
        -	Découper les séquence valides en intervalles successifs.
        -	Sélectionner une séquence aléatoire.

        C4 (La détection à maille fine) :

        C5 (La dégradation de vidéo) :
        Application de bruit synthétique aux VP et VN pour vérifier leurs robustesse.


Cbmi.pdf :

    1)	Un cas de fact-checking classique.


    Voir annexe fact-checking_pipeline


    -	Trouver des affirmations qui valent la peine d’être vérifiées à partir de divers Sources
    -	Vérifier si la revendication sélectionnée a déjà été vérifié précédemment
    -	Rassembler des éléments de preuve pertinents pour aider à comprendre le contexte et la véracité de l’allégation
    -	Décider de la véracité de l’information.

    2)	Organisation des jeux de données.

    voir annexe dataset_organisation
    
    Cette organisation est découpée en deux base de données : Celle issue de factoscope et celle issue des fichier audio/vidéo TV.
    
    Factoscope :
        Les articles de factoscope peuvent nous fournir plusieurs informations :
        -	Le nom de l’auteur 
        -	Une photo du visage de l’auteur.
        -	Le fait énoncé.
        -	Le niveaux de véracité du fait.
        -	Une description contextuel du fait
        -	Liens qui ont permis le fact-checking.
        Un scrapping du site factoscope est effectué sur ce site et a permis créer un jeu de données de plus de 1500 fact-checked. Exemple : voir « newsStorageKeyWords.xml ».

    Les captures vidéo/audio de la station TV :
        Les fichiers audio sont transcrit en texte et les fichiers vidéo en méta data.

    Liens entre les deux jeux de données :
        Les méta data issue de factoscope et les transcriptions des fichiers audio issues de la station TV sont données à un programme NLP. Il  en ressort un fichier xml avec différentes informations (voir Xmltv-db.zip ) :
        -	Id de la news
        -	Le type de news
        -	La chaine
        -	La date
        -	La news

        Le xml qui est issue du NLP est passé au programme de reconnaissance faciale ainsi que les métadata des fichiers vidéo. 
        -	Vérification si c’est la news n’existe pas déjà dans factoscope.
        -	Analyse du contenu 
        -	Détection de la véracité de la news.


Question :

Qu’est ce que les VP et VN ?
Qu'est ce que la détection à maille fine ?
Le git de Alexis ne fonctionne pas.
