import speech_recognition as sr
import moviepy as mp
import os
from path import base_enrichie


def convert_to_wav(audio_path):
    clip = mp.VideoFileClip(audio_path)
    clip.audio.write_audiofile(r"converted.wav")


# --------------------------------------------------------------------
def create_transcript(filePath, fileName):
    convert_to_wav(filePath + fileName)  # r"C:\Users\Jules Courné\OneDrive\Images\Pellicule\test.mp4"
    r = sr.Recognizer()
    audio = sr.AudioFile("converted.wav")
    with audio as source:
        audio_file = r.record(source)
        result = r.recognize_google(audio_data=audio_file, language="fr")
    with open(filePath + "transcript.txt", mode='w') as file:
        file.write("Recognized Speech:")
        file.write("\n")
        file.write(result)


# --------------------------------------------------------------------
def create_all_transcripts():
    for dir_hash in os.listdir(base_enrichie):
        for dir_time in os.listdir(base_enrichie + dir_hash):
            if os.path.exists(base_enrichie + dir_hash + "\\" + dir_time + "\\" + dir_time + "_audio.mp4"):
                if not os.path.exists(base_enrichie + dir_hash + "\\dir_time\\transcript.txt"):
                    create_transcript(base_enrichie + dir_hash + "\\" + dir_time + "\\", "transcript.txt")


# --------------------------------------------------------------------

if __name__ == '__main__':
    create_all_transcripts()
