First , check path.py to define where you want to create the base.

The process to create the base is the following :

############################# file create_csv_files.py##########################################
1) Create the csv which contains hashcodes of politicals programs by calling the "function create_hashcodes_csv()"
2) Create the csv which contains metadata from xmltv-db by calling "create_metadata_csv()"

############################## file create_repositories.py######################################
3) Create the repository structure by calling the function "create_repositories_from_hashcodes()"


############################## file create_xml_files.py########################################
4) Create the xml file associated to each diffusion by calling "create_xml_from_metadata()"

############################## file create_video_segmentation.py########################################
5) Create segmentation of videos associated to each diffusion by calling "segment_video(bool)"
With this function you give a boolean in argument which indicates if multiprocessing is used.
A csv file named "segment_video.csv" is created to see all segmentation made.
For now multiprocessing doesn't allow to gain time but may be on a better computer it will work .

############################## file create_audio_segmentation.py########################################
6) Create segmentation of audios associated to each diffusion by calling "segment_audio(bool)"
With this function you give a boolean in argument which indicates if multiprocessing is used.
A csv file named "segment_audio.csv" is created to see all segmentation made.
For now multiprocessing doesn't allow to gain time but may be on a better computer it will work .

############################## file create_transcripts.py######################################
7) Create a file transcript.txt which contains transcription of the audio of a diffusion.
For the moment a file .wav is created to recognize speeches. It will be deleted.



Explaination of repositories : 

csv_files : contains all csv files needed or created during the creating of the base.
xmltv-db : contains all xmltv files from 2022/02/01 to 2022/05/01


Explaination of files : 

common.py : 
File which permits to navigate between Channel code and channel name.

create_audio_segmentation.py:
File which permits the audio segmentation and put it in the associated folder. Contains severals small functions needed to segment.

create_video_segmentation.py:
File which permits the video segmentation and put it in the associated folder. Contains severals small functions needed to segment.

create_csv_files.py:
File which permits the create of "hashcode.csv" and "metadata.csv".

create_repositories.py:
File which permits to create the repository structure of the base.

create_xml_files.py:
File which permits to create an xml file and put it in the associated folder.

create_transcipts.py:
File which permits to create transcription of audio stocked in the base. (In construction)

path.py:
File which contains path of differents files (csv, xmltv-db, video to segment ..). 