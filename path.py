import os

#Directory of the project
project_path = os.getcwd() + "\\"

#Path of the base to create
base_enrichie = "C:\\Users\\Public\\test\\" + "base_enrichie\\"

#Path of the csv file which contains metadata of the xmltv-database
meta_data_csv = project_path + "csv_files\\meta_data.csv"

#Path of the csv which contains selected programs
programselection_csv = project_path + "csv_files\\programselection.csv"

#Path of the csv files which contains hashcodes of selected programs
hashcodes_csv = project_path + "csv_files\\hashcodes.csv"

#Path of the database xmltv
db_xmltv = project_path + "\\xmltv-db"

#Path of the csv files which contains the videos segmentation
segment_video_csv = project_path + "csv_files\\segment_video.csv"

#Path of videos to segment
video_path = r"D:\Fact_Checking\video_2"

#Path of the csv files which contains the audios segmentation
segment_audio_csv = project_path + "csv_files\\segment_audio.csv"

#Path of audios to segment
audio_path = r"D:\Fact_Checking\audio"

#Path of ffmpeg bin
ffmpeg_path = r"C:\Users\Jules Courné\Downloads\ffmpeg-2022-06-16-git-5242ede48d-essentials_build\bin"
